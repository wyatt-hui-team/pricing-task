# Pricing Task #

### Summary ###
Flexible engine that allows us to add rules which implements types of logic, applying pricing tweaks/adjustments along the way.

### Development
* Install back-end packages

    ```
    composer install
    ```

* Migrate database

    ```
    php artisan migrate
    ```

### Team contacts
* Wyatt Hui (wyatt.hui.1104@gmail.com)
